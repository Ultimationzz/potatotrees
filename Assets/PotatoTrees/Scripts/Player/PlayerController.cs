﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;

public class PlayerController : MonoBehaviour
{

    public CharacterController Controller;
    private NetworkIdentity Network;
    public float Speed = 5f;
	// Use this for initialization
	void Start ()
    {
        Controller = GetComponent<CharacterController>();
        Network = GetComponent<NetworkIdentity>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        if (Network.isLocalPlayer)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");
            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            Controller.SimpleMove(movement * Speed);
        }

    }
}
