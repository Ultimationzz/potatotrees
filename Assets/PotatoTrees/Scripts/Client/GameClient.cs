﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameClient : MonoBehaviour
{

    void Start()
    {
        if (!ServerManager.IsHeadless)
        {

            NetworkManager.singleton.StartClient();
        }
       
    }
}
