﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Rendering;

public class ServerManager : MonoBehaviour
{
    public static bool IsHeadless => SystemInfo.graphicsDeviceType == GraphicsDeviceType.Null;
    public static bool IsServer => IsHeadless;
    void Start()
    {
        Application.runInBackground = true;
        DontDestroyOnLoad(gameObject);
        if (IsHeadless)
        {
            StartServer();
        }
    }
    /// <summary>
    /// The server will only start when the client is ran in headless mode. (No GFX output, can be used cross platform)
    /// </summary>
    void StartServer()
    {
        NetworkManager.singleton.StartServer();
    }
}
