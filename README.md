# potato trees

the next big game hit. by instinct, ultimation1 and domsson.

## project outline (by instinct)

- 3D
- multiplayer
- coop (pve) and pvp
- procedurally generated world
- realistic / postapocalyptic setting
- there are humans, cyborgs and ghosts
- endless gameplay (no "beating" the game)
- has elements of survival, city builder, adventure and rpg
- uses unity

## game concept idea (by domsson)

**story**

you wake up. you don't remember anything. where are you? why are you? 
and most important: what are you? human, your brain says. but then you 
investigate your body and it doesn't seem human at all. you are a cyborg.

you explore the environment. it seems abandoned for the most part. 
soon enough you'll meet others like you. slowly, the picture becomes 
clearer. something must have happened. maybe 10 years ago, maybe 100. 
in any case, all humans seem to be gone. everyone you meet is a cyborg. 
everyone tells a story just like yours. 

through talking to others and by exploring hints in the environment, you 
figure out that whatever has happened has wiped out most biological life. 

however, many people have been digitalized. their brains scanned and 
their minds and personalities transferred into computers. through cyborg 
bodies, built with a mixture of lab-grown organs as well electronic and 
mechanical parts, some of these people have been brought back to 
consciousness. you are one of them.

collectively, the cyborg 'survivors' attempt to bring natural human 
beings back to life. as an intermediary step, the active cyborgs strive 
to find and assemble resources required to increase their own numbers. 
this is how you have been awaken - other cyborgs managed to assemble yet 
another cybernetic body, then drew one of the digitalized minds from a 
database, based partially on random selection, partially on an algorithm 
that will try to identify especially cabable minds.

additionally, the 'databases', that is, the computers that contain the 
minds, enable you to consult with minds that have not yet been copied 
into a body. these digital minds are, for their lack of a body, 
commonly refered to as "ghosts".

big efforts are also being put in exploration. this is for two reasons. 
first, in order to operate the database, as well as the cybernetic 
bodies of the survivors, energy is required. with most biological life 
gone, there is but one source of energy: potato trees. no one managed 
to figure out how they came about or why they are still around, but they 
can be found throughout the world and they provide sufficient amounts of 
energy. second, some ghosts have prophesized that there are still real 
humans left in the world. if you could manage to find them, then the 
rekindling of manking would be an actual possibility.

some cyborgs, however, have a different agenda. they believe that the 
event that wiped out life was caused by humans, and in order to prevent 
humans from ever causing such an event again, they shall never walk the 
earth again. they want to prevent the creation of more pro-human cyborgs 
and instead want to infiltrate all databases they can find. on success, 
they install a virus that modifies the ghosts contained within to ensure 
they will be anti-human upon bringing them into the world. this way, 
more anti-human cyborgs are created.

a war between pro- and anti-human cyborgs is about to commence.

at the same time, everyone is fighting a battle of survival against the 
environment. with most life gone, nature has turned into a creature 
itself. the air quality is too bad to sustain normal human life in most 
areas. only where enough potato trees have been planted is the air good 
enough for non-cyborg organisms again. seemingly at random, nature 
brings periods of draught with heavy sand storms, periods of snow and 
blizzards and other dangerous events that require careful preparation in 
order to make it through alive and without too much damage.

**gameplay**

- explore the environment in search for materials, resources, databases,
  potato trees, humans as well as buildings that could be restored and 
  made into homes other useful facilities
- plant more potato trees in order to ensure energy supply and to 
  improve air quality to create a habitable environment for humans
- make sure your cyborg body and the databses under your control keep 
  operating by providing them with the required energy from potato trees
- find a place to call home, restore as best as you can and improve and 
  decorate it over time
- find allies, build a community, maybe move in together or close by - 
  together, you are stronger and can achieve more
- protect or infiltrate found databases, depending on who you fight for
- try to find clues in an attempt to uncover what has happened that 
  wiped out mankind and most life 
- reshape the face of the city by restoring abandoned builings or by 
  even building new structures from scratch
- protect yourself against attacks from the other cyborg party or even 
  launch your own attacks
- find unique parts that enable you to upgrade or otherwise modify your 
  cyborg body in an attempt to gain new abilities or improve exisiting 
  ones

**look & feel**

in order to make development and asset creation easier, we go with a 
deliberately limited look and feel. world and characters will be rather 
low poly. this will also help to keep performance in check, which could 
be challenging with a rather large, prcedurally generated world. plus, 
when using procedural generation, low-poly should help keep the required 
algorithms rather simple. think somewhere between high-end PS1 and 
low-end PS2 titles. textures will be rather low-res as well, which, 
again, will help the procedual approach. in an attempt to create a retro 
feeling, we could even use a palette-based approach for textures. 

in order to make exploration more interesting, the atmosphere more 
depressing and performance less of a concern, fog could be used rather 
intensly. it would keep the view distance down while creating an almost 
scary environment where both enemies or precious resources could wait 
behind every corner. 

think a mixture of silent hill 1 and 2.

